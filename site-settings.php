<?php

return [
    'config_dir'  => 'novum.bri',
    'namespace'   => 'NovumBri',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'belastingdienst.demo.novum.nu',
    'dev_domain'  => 'belastingdienst.demo.novum.nuidev.nl',
    'test_domain' => 'belastingdienst.test.demo.novum.nu',
];
